#include "kstring.h"

void * kmemcpy(void * to, const void * from, uint32_t size)
{
	uint32_t i = 0;

	while(i < size)
		((uint8_t *)to)[i] = ((uint8_t *)from)[i];

	return to;
}

uint32_t kstrlen(const char * str)
{
	uint32_t len = 0;
	while(str[len] != '\0') {
		len += 1;
	}

	return len;
}

char* kstrcpy(char * to, const char * from)
{
	uint32_t str_len = kstrlen(from) + 1;
	return kmemcpy(to, from, str_len);
}

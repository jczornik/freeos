#ifndef __TYPEDEFS__
#define __TYPEDEFS__

typedef char int8_t;
typedef unsigned char uint8_t;
typedef short int16_t;
typedef unsigned short uint16_t;
typedef int int32_t;
typedef unsigned int uint32_t;
typedef long int int64_t;
typedef unsigned long int uint64_t;
typedef uint16_t size_t;

#endif

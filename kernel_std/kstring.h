#ifndef __KSTRING__
#define __KSTRING__

#include "typedefs.h"

void * kmemcpy(void *, const void *, uint32_t);
uint32_t kstrlen(const char *);
char * kstrcpy(char *, const char *);


#endif

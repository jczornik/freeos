#include "console.h"
#include "../kernel_std/kstring.h"
#include "console_err.h"

#define VGA_ADDR 0xB8000
#define VGA_WIDTH 80
#define VGA_HEIGHT 25
#define VGA_LENGTH VGA_WIDTH * VGA_HEIGHT
#define NEW_LINE '\n'

static uint16_t * vga_buff = (uint16_t*)VGA_ADDR;
static const uint8_t console_def_color = VGA_COLOR_BLACK << 4 | VGA_COLOR_WHITE;
static uint8_t console_color = console_def_color;

static uint16_t kprintf_pos = 0;

static uint8_t console_shift_to_new_line()
{
	uint8_t res = 0;
	uint8_t active_line = kprintf_pos / VGA_WIDTH + 1;
	if(active_line + 1 > VGA_HEIGHT)
		res = console_clear();
	else
		kprintf_pos = active_line * VGA_WIDTH;
	return res;
}

static uint8_t console_putc_at(char letter, uint32_t position)
{
	if(position >= VGA_LENGTH)
		return VGA_VALUE_RANGE;

	if(letter == NEW_LINE)
		console_shift_to_new_line();
	else
		vga_buff[position] = letter | console_color << 8;
	return 0;
}

static int8_t console_clear_line(uint8_t line)
{
	uint8_t res = 0;

	if(line > VGA_HEIGHT) {
		res = VGA_VALUE_RANGE;
		goto END;
	}

	uint8_t console_color_tmp = console_color;
	console_set_color(VGA_COLOR_BLACK, VGA_COLOR_BLACK);

	uint8_t i = 0;
	while(i < VGA_WIDTH) {
		if((res = console_putc_at(' ', line * VGA_WIDTH + i)) != 0)
			goto RESTORE;
		i += 1;
	}

RESTORE:
	console_color = console_color_tmp;

END:
	return res;
}

uint8_t console_init()
{
	return console_clear();
}

uint8_t kprintf(const char * str)
{
	uint8_t res = 0;

	uint32_t len = kstrlen(str);
	uint32_t i = 0;

	while(i < len) {
		kprintf_pos += 1;
		if(kprintf_pos >= VGA_LENGTH) {
			kprintf_pos = 0;
			if((res = console_clear()) != 0)
				goto END;
		}
		if((res = console_putc_at(str[i], kprintf_pos)) != 0)
			goto END;
		i += 1;
	}

END:
	return res;
}

void console_set_color(vga_color bg, vga_color fg)
{
	console_color = (uint8_t)bg << 4 | (uint8_t)fg;
}

uint8_t console_clear()
{
	uint8_t res = 0;

	uint8_t console_color_tmp = console_color;
	console_set_color(VGA_COLOR_BLACK, VGA_COLOR_BLACK);

	uint8_t height_pos = 0;

	while(height_pos < VGA_HEIGHT) {
		res = console_clear_line(height_pos);

		if(res != 0)
			goto RESTORE;
		height_pos += 1;
	}

RESTORE:
	console_color = console_color_tmp;
	return res;
}

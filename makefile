ASFLAGS=-32
GCCFLAGS=-m32 -std=gnu99 -ffreestanding -O2 -Wall -Wextra
LDPARAMS=-melf_i386

SRC = $(wildcard *.c)\
      $(wildcard kernel_std/*.c)\
      $(wildcard console/*.c)

OBJECTS = $(SRC:.c=.o) load.o

%.o: %.c
	gcc $(GCCFLAGS) -o $@ -c $<

%.o: %.s
	as $(ASFLAGS) -o $@ $<

all: linker.ld $(OBJECTS)
	gcc -T linker.ld -m32 -o kernel.bin -ffreestanding -O2 -nostdlib $(OBJECTS)

clean:
	@rm $(OBJECTS)
	@rm kernel.bin

cos:
	ld $(LDPARAMS) -T $< -o kernel.bin $(OBJECTS)
